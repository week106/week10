
Future<String> fetchUserOrder() =>
    Future.delayed(
      const Duration(seconds: 4),
      () => 'Large Latte',
    );

Future<void> printOrderMessage() async {
  print('Awaitig user data ...');
  var order = await fetchUserOrder();
  print('Your order is : $order');  
}
void main() async{
  countSeconds(4);
  await printOrderMessage();
}

void countSeconds(int s) {
  for (var i = 1; i <= s; i++) {
    Future.delayed(Duration(seconds: i), () => print(i));
  }
}